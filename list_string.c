#include <stdlib.h>
#include <utlist.h>
#include "common.h"

typedef struct Entry {
    char *value;
    struct Entry *next;
    struct Entry *prev;
} Entry;

typedef struct ListString {
    Entry *head;
    size_t size;
} ListString;

ListString* list_string_new() {
    ListString *result = (ListString *) MALLOC(sizeof(ListString));
    result->size = 0;
    result->head = NULL;
    return result;
}

void list_string_clear(ListString *lst) {
    Entry *head = lst->head;
    Entry *elem = NULL;
    Entry *tmp = NULL;
    DL_FOREACH_SAFE(head, elem, tmp) {
        DL_DELETE(head, elem);
        free(elem);
    }
    lst->head = NULL;
    lst->size = 0;
}

void list_string_free(ListString *lst) {
    list_string_clear(lst);
    free(lst);
}

void list_string_free_all(ListString *lst) {
    Entry *elt;
    Entry *tmp;
    Entry *head = lst->head;
    DL_FOREACH_SAFE(head, elt, tmp){
        DL_DELETE(head, elt);
        free(elt->value);
        free(elt);
    }
    free(lst);
}


int list_string_size(ListString *lst) {
    return lst->size;
}

void list_string_to_array(ListString *lst, char **dst) {
    Entry *elem = NULL;
    Entry *head = lst->head;
    size_t i = 0;
    DL_FOREACH(head, elem) {
        dst[i] = elem->value;
        i++;
    }
}

void list_string_append(ListString *lst, char *value) {
    Entry *elem = (Entry *) MALLOC(sizeof(Entry));
    elem->value = value;
    DL_APPEND(lst->head, elem);
    lst->size++;
}

char* list_string_remove(ListString *lst, const char *value) {
    Entry *elem;
    Entry *tmp;
    Entry *head = lst->head;
    DL_FOREACH_SAFE(head, elem, tmp) {
        if (!strcmp(value, elem->value)) {
            char *result = elem->value;
            DL_DELETE(head, elem);
            free(elem);
            return result;
        }
    }
    lst->head = head;
    lst->size--;
    return NULL;
}

void list_string_add_all(ListString *dst, ListString *src) {
    Entry *elem = NULL;
    Entry *src_lst = src->head;
    Entry *dst_lst = dst->head;
    DL_FOREACH(src_lst, elem) {
        Entry *entry = (Entry *) MALLOC(sizeof(Entry));
        entry->value = elem->value;
        DL_APPEND(dst_lst, entry);
    }
    dst->head = dst_lst;
    dst->size += src->size;
}

ListString *list_string_copy(ListString *lst) {
    ListString *result = list_string_new();
    list_string_add_all(result, lst);
    return result;
}

void list_string_drain_to(ListString *lst, char **dst) {
    size_t i = 0;
    Entry *head = lst->head;
    Entry *elem = NULL;
    Entry *tmp = NULL;
    DL_FOREACH_SAFE(head, elem, tmp) {
        dst[i] = elem->value;
        DL_DELETE(head, elem);
        free(elem);
        i++;
    }
    lst->head = NULL;
    lst->size = 0;
}
