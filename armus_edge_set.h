#ifndef __ARMUS_EDGE_SET_H
#define __ARMUS_EDGE_SET_H
#ifdef __cplusplus
extern "C" {
#endif

typedef struct ArmusEdgeSet {
    char** predecessors;
    size_t predecessors_size;
    char** successors;
    size_t successors_size;
} ArmusEdgeSet;

#ifdef __cplusplus
}
#endif
#endif
