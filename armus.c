#include "armus.h"
#define PREFIX "armus"
#define LAST_TASK_ID "last_task_id" 
#define ALL_TASKS "tasks"
#define TASK "task"
#define SEP ":"
#define SUCCESSORS "successors"
#define PREDECESSORS "predecessors"
#define MAX_RETRIES 100

int armus_create_task(redisContext *c, ArmusTid* tid) {
    redisReply *reply;
    long long added;
    ArmusTid task_id;
    int retries = MAX_RETRIES;
    int is_ok;
    do {
        reply = redisCommand(c, "INCR %s:%s", PREFIX, LAST_TASK_ID);
        task_id = reply->integer;
        is_ok = reply->type == REDIS_REPLY_INTEGER;
        freeReplyObject(reply);
        if (!is_ok) {
            return is_ok;
        }
        reply = redisCommand(c, "SADD %s:%s %s:%s:%lld", PREFIX, ALL_TASKS, PREFIX, TASK, task_id);
        added = reply->integer;
        is_ok = reply->type == REDIS_REPLY_INTEGER;
        freeReplyObject(reply);
        retries--;
    } while (is_ok && !added && retries >= 0);
    if (is_ok && retries > 0) {
        *tid = task_id;
    }
    return is_ok && retries > 0;
}

int armus_free_task(redisContext *c, ArmusTid task_id) {
    int successful;
    redisReply *reply;
    reply = redisCommand(c, "SREM %s:%s %s:%s:%lld", PREFIX, ALL_TASKS, PREFIX, TASK, task_id);
    successful = reply->integer == 1;
    freeReplyObject(reply);
    if (successful) {
        reply = redisCommand(c, "DEL %s:%s:%lld:%s %s:%s:%lld:%s", PREFIX, TASK, task_id, SUCCESSORS, PREFIX, TASK, task_id, PREDECESSORS);
        freeReplyObject(reply);
    }
    return successful;
}

static int armus_task_clear_field(redisContext *c, ArmusTid task_id, const char* field) {
    int is_ok;
    redisReply *reply;
    reply = redisCommand(c, "DEL %s:%s:%lld:%s", PREFIX, TASK, task_id, field);
    is_ok = reply->type == REDIS_REPLY_STATUS;
    freeReplyObject(reply);
    return is_ok;
}
static long long armus_task_field_push(redisContext *c, ArmusTid task_id, const char* field, const char* resource) {
    long long count;
    redisReply *reply;
    reply = redisCommand(c, "LPUSH %s:%s:%lld:%s %s", PREFIX, TASK, task_id, field, resource);
    count = reply->integer;
    freeReplyObject(reply);
    return count;
}

int armus_task_set_blocked(redisContext *c, ArmusTid task_id, ArmusEdgeSet *edgeSet) {
    if (edgeSet == NULL) {
        int left = armus_task_clear_field(c, task_id, SUCCESSORS);
        int right = armus_task_clear_field(c, task_id, PREDECESSORS);
        return left && right;
    }
    int is_ok = 1;
    int i;
    for (i = 0; is_ok && i < edgeSet->predecessors_size; i++) {
        is_ok = armus_task_field_push(c, task_id, PREDECESSORS, edgeSet->predecessors[i]);
    }
    for (i = 0; is_ok && i < edgeSet->successors_size; i++) {
        is_ok = armus_task_field_push(c, task_id, SUCCESSORS, edgeSet->successors[i]);
    }
    return is_ok;
}
