#ifndef __ARMUS_TASK_H
#define __ARMUS_TASK_H
#ifdef __cplusplus
extern "C" {
#endif

#include "armus_edge_set.h"

#include "list_string.h"
    
typedef struct ArmusTask ArmusTask;
    
typedef void (*ArmusTaskSetBlocked)(ArmusTask *self, ArmusEdgeSet *edge_set);

typedef struct ArmusTask {
    int barrier;
    ListString *held_locks;
    ArmusTaskSetBlocked set_blocked_func;
    void *user_data;
} ArmusTask;

ArmusTask* armus_task_new(ArmusTaskSetBlocked set_blocked_func);

void armus_task_free(ArmusTask *task);

void armus_task_enter_critical(ArmusTask *task, const char*name);
void armus_task_before_lock(ArmusTask *task, const char *name);
void armus_task_after_lock(ArmusTask *task, const char *name);

void armus_task_before_unlock(ArmusTask *task, const char *name);
void armus_task_after_unlock(ArmusTask *task, const char *name);

void armus_task_before_barrier(ArmusTask *task);
void armus_task_after_barrier(ArmusTask *task);

#ifdef __cplusplus
}
#endif
#endif
