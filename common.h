#ifndef __COMMON_H
#define __COMMON_H
#include <string.h>
#include <stdlib.h> // exit()

static void * MALLOC(size_t size) {
    void *v = malloc(size);  
    if(v == NULL){
        exit(1);
    }
    return v;
}
static void foreach_free(void **arr, size_t size) {
    size_t i;
    for (i = 0; i < size; i++) {
        free(arr[i]);
    }
}

static char *string_copy(const char *buff) {
    if (buff == NULL) {
        return NULL;
    }
    size_t buff_size = strlen(buff) + 1;
    char *cpy = (char *) MALLOC(sizeof(char) * buff_size);
    memcpy(cpy, buff, buff_size);
    return cpy;
}

static void array_string_copy(char **dst, char **src, size_t count) {
    size_t i;
    for (i = 0; i < count; i++) {
        dst[i] = string_copy(src[i]);
    }
}
#endif
