#define _GNU_SOURCE
#include <dlfcn.h>
#include <omp.h>
#include "oarmus.h"
#include <stdlib.h>

#include <stdio.h>

void omp_set_lock(omp_lock_t *lock) {
    static OmpSetLock proceed = NULL;
    printf("set\n");
    if (!proceed) {
        proceed = dlsym(RTLD_NEXT,"omp_set_lock");
    }
    armus_omp_set_lock(lock, proceed);
}
void omp_unset_lock(omp_lock_t *lock) {
    static OmpUnsetLock proceed = NULL;
    printf("unset\n");
    if (!proceed) {
        proceed = dlsym(RTLD_NEXT,"omp_unset_lock");
    }
    armus_omp_unset_lock(lock, proceed);
}
