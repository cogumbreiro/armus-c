#include "armus_task.c"
#include "seatest.h"

ArmusEdgeSet *EDGE_SET = NULL;

void dummy_set_blocked(ArmusTask *self, ArmusEdgeSet *edge_set) {
    EDGE_SET = copy_edge_set(edge_set);
}

static void assert_array_string_equal(char **arr1, size_t count1, char **arr2, size_t count2) {
    size_t i;
    assert_int_equal(count1, count2);
    for (i = 0; i < count1; i++) {
        assert_string_equal(arr1[i], arr2[i]);
    }
}

static void assert_edge_set_equal(ArmusEdgeSet *edge1, ArmusEdgeSet *edge2) {
    if (edge1 == NULL || edge2 == NULL) {
        assert_true(edge1 == edge2);
        return;
    }
    assert_array_string_equal(edge1->successors, edge1->successors_size, edge2->successors, edge2->successors_size);
    assert_array_string_equal(edge1->predecessors, edge1->predecessors_size, edge2->predecessors, edge2->predecessors_size);
}

///////////////////////////////////////////////////////////////////

void alloc_free() {
    ArmusTask* task = armus_task_new(&dummy_set_blocked);
    armus_task_free(task);
}

void test_before_barrier() {
    assert_true(EDGE_SET == NULL);
    ArmusTask* task = armus_task_new(&dummy_set_blocked);
    list_string_append(task->held_locks, string_copy("foo"));
    list_string_append(task->held_locks, string_copy("bar"));
    armus_task_before_barrier(task);
    // b:0 > (foo, bar, b:1)
    char *succ[1];
    succ[0] = "b:0";
    char *pred[3];
    pred[0] = "foo";
    pred[1] = "bar";
    pred[2] = "b:1";
    ArmusEdgeSet expected = {
        .successors = succ,
        .successors_size = 1,
        .predecessors = pred,
        .predecessors_size = 3
    };
    assert_edge_set_equal(&expected, EDGE_SET);
    free_edge_set(EDGE_SET);
    EDGE_SET = NULL;
    armus_task_free(task);
}

void test_lock_before() {
    assert_true(EDGE_SET == NULL);
    ArmusTask* task = armus_task_new(&dummy_set_blocked);
    armus_task_enter_critical(task, "foo");
    armus_task_enter_critical(task, "bar");
    armus_task_before_barrier(task);
    // b:0 > (foo, bar, b:1)
    char *succ[1];
    succ[0] = "b:0";
    char *pred[3];
    pred[0] = "foo";
    pred[1] = "bar";
    pred[2] = "b:1";
    ArmusEdgeSet expected = {
        .successors = succ,
        .successors_size = 1,
        .predecessors = pred,
        .predecessors_size = 3
    };
    assert_edge_set_equal(&expected, EDGE_SET);
    free_edge_set(EDGE_SET);
    EDGE_SET = NULL;
    armus_task_free(task);
}

void test_before_lock() {
    assert_true(EDGE_SET == NULL);
    ArmusTask* task = armus_task_new(&dummy_set_blocked);
    armus_task_enter_critical(task, "foo");
    armus_task_enter_critical(task, "bar");
    armus_task_before_barrier(task);
    // b:0 > (foo, bar, b:1)
    char *succ[1];
    succ[0] = "b:0";
    char *pred[3];
    pred[0] = "foo";
    pred[1] = "bar";
    pred[2] = "b:1";
    ArmusEdgeSet expected = {
        .successors = succ,
        .successors_size = 1,
        .predecessors = pred,
        .predecessors_size = 3
    };
    assert_edge_set_equal(&expected, EDGE_SET);
    free_edge_set(EDGE_SET);
    EDGE_SET = NULL;
    armus_task_free(task);
}

void test_gen_prec() {
    ArmusTask* task = armus_task_new(&dummy_set_blocked);
    StringArray result = generate_precedes(task);
    assert_int_equal(1, result.size);
    assert_string_equal("b:0", result.value[0]);
    foreach_free((void**)result.value, result.size);
    free(result.value);
    armus_task_free(task);
}

void all_tests() {
    test_fixture_start();
    run_test(test_lock_before);
    run_test(test_gen_prec);
    run_test(alloc_free);
    run_test(test_before_barrier);
    test_fixture_end();       
}

int main( int argc, char** argv ) {
    return run_tests(all_tests);
}
