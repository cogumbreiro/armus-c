#include "common.h"
#define REDIS_DEFAULT_PORT 6379
#define REDIS_DEFAULT_IP "localhost"
#include <omp.h>
#include <hiredis.h>
#include "armus_edge_set.h"
#include "armus.h"
#include "armus_task.h"
#include "oarmus.h"

typedef struct OMPArmus {
    ArmusTid task_id;
} OMPArmus;

__thread ArmusTask *CURR = NULL;

static redisContext *connect() {
    redisContext *c = redisConnect(REDIS_DEFAULT_IP, REDIS_DEFAULT_PORT);
    if (c == NULL || c->err) {
        if (c) {
            printf("Connection error: %s\n", c->errstr);
            redisFree(c);
        } else {
            printf("Connection error: can't allocate redis context\n");
        }
        exit(1);
    }
    return c;
}

static void our_set_blocked(ArmusTask *self, ArmusEdgeSet *edge_set) {
    OMPArmus *data = (OMPArmus *) self->user_data;
    redisContext *c = connect();
    armus_task_set_blocked(c, data->task_id, edge_set);
    redisFree(c);
}

static void armus_omp_init() {
    if (CURR != NULL) {
        return;
    }
    ArmusTask *tinf = armus_task_new(&our_set_blocked);
    OMPArmus *data = (OMPArmus*) MALLOC(sizeof(OMPArmus));
    
    redisContext *c = connect();
    int is_ok = armus_create_task(c, &data->task_id);
    if (!is_ok) {
        if (c->errstr) {
            printf("could not init redis:%s \n", c->errstr);
        } else {
            printf("could not init redis (no error message given)\n");
        }
        exit(1);
    }
    redisFree(c);
    tinf->user_data = (void *) data;
    CURR = tinf;
}

void armus_omp_set_lock(omp_lock_t *lock, OmpSetLock proceed) {
    armus_omp_init();
    char name[64];
    snprintf(name, sizeof(name), "%p", lock);
    armus_task_before_lock(CURR, name);
    proceed(lock);
    armus_task_after_lock(CURR, name);
    armus_task_enter_critical(CURR, name);
}

void armus_omp_unset_lock(omp_lock_t *lock, OmpUnsetLock proceed) {
    armus_omp_init();
    char name[64];
    snprintf(name, sizeof(name), "%p", lock);
    armus_task_before_unlock(CURR, name);
    proceed(lock);
    armus_task_after_unlock(CURR, name);
}
