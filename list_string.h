#ifndef __LIST_STRING_H
#define __LIST_STRING_H
#ifdef __cplusplus
extern "C" {
#endif
typedef struct ListString ListString;
ListString* list_string_new();
void list_string_free(ListString *lst);
void list_string_free_all(ListString *lst);
int list_string_size(ListString *lst);
void list_string_to_array(ListString *lst, char**array);
void list_string_clear(ListString *lst);
void list_string_drain_to(ListString *lst, char **array);
void list_string_append(ListString *lst, char *value);
char *list_string_remove(ListString *lst, const char *value);
void list_string_add_all(ListString *dst, ListString *src);
#ifdef __cplusplus
}
#endif
#endif
