#ifndef __ARMUS_H
#define __ARMUS_H
#ifdef __cplusplus
extern "C" {
#endif

#include <hiredis.h>

#include "armus_edge_set.h"

typedef long long ArmusTid;

typedef struct ArmusConnection {
    redisContext *ctx;
} ArmusConnection;

/*
 * Tries to create a task. Returns true if the task was created. The task id is returned in parameter task_id.
 */
int armus_create_task(redisContext *c, ArmusTid *task_id);
/**
 * Frees the task id. Returns the success of the operation.
 */
int armus_free_task(redisContext *c, ArmusTid task_id);
/**
 * Sets the blocked state of the given task. Set NULL to mark the task as unblocked.
 * Returns the success of the operation.
 */
int armus_task_set_blocked(redisContext *c, ArmusTid task_id, ArmusEdgeSet *edgeSet);

#ifdef __cplusplus
}
#endif
#endif
