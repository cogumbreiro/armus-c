#include "list_string.c"
#include "seatest.h"

void test_new_list() {
    ListString *lst = list_string_new();
    assert_true(lst != NULL);
    assert_int_equal(0, lst->size);
    assert_true(lst->head == NULL);
    list_string_free(lst);
}

void test_append() {
    ListString *lst = list_string_new();
    list_string_append(lst, "foo");
    assert_int_equal(1, lst->size);
    assert_true(lst->head != NULL);
    list_string_free(lst);
}

void test_append_alloced() {
    ListString *lst = list_string_new();
    list_string_append(lst, "foo");
    list_string_append(lst, "bar");
    assert_int_equal(2, lst->size);
    list_string_clear(lst);
    assert_int_equal(0, lst->size);
    assert_true(lst->head == NULL);
    list_string_free(lst);
}

void test_to_array() {
    ListString *lst = list_string_new();
    list_string_append(lst, "foo");
    list_string_append(lst, "bar");
    char *dst[2];
    list_string_to_array(lst, dst);
    assert_string_equal("foo", dst[0]);
    assert_string_equal("bar", dst[1]);
    list_string_free(lst);
}

void test_drain_to_array() {
    ListString *lst = list_string_new();
    list_string_append(lst, "foo");
    list_string_append(lst, "bar");
    char *dst[2];
    list_string_drain_to(lst, dst);
    assert_string_equal("foo", dst[0]);
    assert_string_equal("bar", dst[1]);
    list_string_free(lst);
}

void add_all() {
    ListString *lst1 = list_string_new();
    ListString *lst2 = list_string_new();
    list_string_append(lst1, "foo");
    list_string_append(lst2, "bar");
    list_string_add_all(lst1, lst2);
    char *dst[2];
    list_string_to_array(lst1, dst);
    assert_string_equal("foo", dst[0]);
    assert_string_equal("bar", dst[1]);
    list_string_free(lst1);
    list_string_free(lst2);
}

void all_tests() {
    test_fixture_start();
    run_test(test_new_list);
    run_test(test_append);
    run_test(test_append_alloced);
    run_test(test_to_array);
    run_test(test_drain_to_array);
    run_test(add_all);
    test_fixture_end();       
}

int main( int argc, char** argv ) {
    return run_tests(all_tests);
}
