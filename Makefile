CC = gcc
CFLAGS = -I/usr/include/hiredis -Iuthash/src/ -Wall -g -fPIC

all: example test_list_string test_armus_task omp_bug5 oa.so

test: test1

test1: test_armus_task
	valgrind --leak-check=full -v ./test_armus_task

omp_bug5: omp_bug5.c oarmus.o armus_task.o list_string.o armus.o
	$(CC) $< -fopenmp oarmus.o armus_task.o list_string.o armus.o -lhiredis $(CFLAGS) -o $@

oa.so: oa.c oarmus.o armus_task.o list_string.o armus.o
	$(CC) $< -shared -ldl -fPIC -fopenmp oarmus.o armus_task.o list_string.o armus.o -lhiredis $(CFLAGS) -o $@

seatest.o: seatest.c seatest.h
	$(CC) -c $< $(CFLAGS) -o $@

list_string.o: list_string.c list_string.h
	$(CC) -c $< $(CFLAGS) -o $@

test_list_string: test_list_string.c list_string.h list_string.c seatest.o
	$(CC) $< seatest.o $(CFLAGS) -o $@

test_armus_task: test_armus_task.c list_string.o seatest.o armus_task.c common.h
	$(CC) $< seatest.o list_string.o $(CFLAGS) -o $@

armus_task.o: armus_task.c armus_task.h armus_edge_set.h list_string.o
	$(CC) -c $< $(CFLAGS) -o $@

oarmus.o: oarmus.c oarmus.h
	$(CC) -c $< -lhiredis $(CFLAGS) -o $@

armus.o: armus.c armus.h
	$(CC) -c $< -lhiredis $(CFLAGS) -o $@

example: example.c armus.o armus_task.o list_string.o
	$(CC) $< armus.o armus_task.o list_string.o -lhiredis $(CFLAGS) -o $@

clean:
	rm -f *.o example test_list_string test_armus_task
