#include <stdio.h>
#include <stdlib.h>
#include "armus.h"
#include "armus_task.h"
#include "list_string.h"
#include "common.h"

typedef struct StringArray {
    char **value;
    size_t size;
} StringArray;

static StringArray slist_singleton(char* str) {
    StringArray result = {NULL, -1};
    result.value = (char**) MALLOC(sizeof(char *) * 1);
    result.size = 1;
    result.value[0] = str;
    return result;
}

// 32767
#define MAX_INT_TO_STR_LEN (5 + 1)
#define MAX_BNAME_LEN (MAX_INT_TO_STR_LEN + 1)

static char* barrier_to_string(int bid) {
    char *bname = (char *) MALLOC(sizeof(char) * MAX_BNAME_LEN);
    snprintf(bname, MAX_BNAME_LEN, "b:%d", bid);
    return bname;
}

#if 0
static ArmusEdgeSet to_edge_set(StringArray *predecessors, StringArray *successors) {
    ArmusEdgeSet result = {
        .predecessors = predecessors->value,
        .predecessors_size = predecessors->size,
        .successors = successors->value,
        .successors_size = successors->size
    };
    return result;
}
#endif

static ArmusEdgeSet* new_edge_set(StringArray *predecessors, StringArray *successors) {
    ArmusEdgeSet *result = (ArmusEdgeSet*) MALLOC(sizeof(ArmusEdgeSet));
    result->predecessors = predecessors->value;
    result->predecessors_size = predecessors->size;
    result->successors = successors->value;
    result->successors_size = successors->size;
    return result;
}

static ArmusEdgeSet *copy_edge_set(ArmusEdgeSet *edge_set) {
    if (edge_set == NULL || edge_set->predecessors_size < 0 || edge_set->successors_size < 0) {
        return NULL;
    }
    ArmusEdgeSet *result = (ArmusEdgeSet*) MALLOC(sizeof(ArmusEdgeSet));
    result->predecessors = (char **) MALLOC(sizeof(char*) * edge_set->predecessors_size);
    result->successors = (char **) MALLOC(sizeof(char*) * edge_set->successors_size);
    result->successors_size = edge_set->successors_size;
    result->predecessors_size = edge_set->predecessors_size;
    array_string_copy(result->successors, edge_set->successors, edge_set->successors_size);
    array_string_copy(result->predecessors, edge_set->predecessors, edge_set->predecessors_size);
    return result;
}

static void free_edge_set(ArmusEdgeSet *edge_set) {
    if (edge_set != NULL) {
        foreach_free((void**)edge_set->predecessors, edge_set->predecessors_size);
        free(edge_set->predecessors);
        foreach_free((void**)edge_set->successors, edge_set->successors_size);
        free(edge_set->successors);
        free(edge_set);
    }
}

static StringArray generate_precedes(ArmusTask *src) {
    StringArray result = {
        .value = NULL,
        .size = -1
    };
    char* b = barrier_to_string(src->barrier);
    size_t locks_count = list_string_size(src->held_locks);
    // make room for the barrier
    char **arr = (char **) MALLOC(sizeof(char*) * (1 + locks_count));
    list_string_to_array(src->held_locks, arr);
    // copy each string in the array
    array_string_copy(arr, arr, locks_count);
    arr[locks_count] = b;
    result.value = arr;
    result.size = locks_count + 1;
    return result;
}

static void set_blocked(ArmusTask *src, ArmusEdgeSet* edge_set) {
    src->set_blocked_func(src, edge_set);
}

// Barrier

void armus_task_before_barrier(ArmusTask *task) {
    char *b = barrier_to_string(task->barrier);
    StringArray succ = slist_singleton(b);
    task->barrier++;
    StringArray pred = generate_precedes(task);
    ArmusEdgeSet* result = new_edge_set(&pred, &succ);
    set_blocked(task, result);
    free_edge_set(result);
}

void armus_task_after_barrier(ArmusTask *task) {
    set_blocked(task, NULL);
}

// Lock

void armus_task_before_lock(ArmusTask *task, const char *lock) {
    char *our_lock = string_copy(lock);
    StringArray succ = slist_singleton(our_lock);
    StringArray pred = generate_precedes(task);
    ArmusEdgeSet* result = new_edge_set(&pred, &succ);
    set_blocked(task, result);
    free_edge_set(result);
}

void armus_task_after_lock(ArmusTask *task, const char *lock) {
    set_blocked(task, NULL);
}

void armus_task_enter_critical(ArmusTask *task, const char *lock) {
    char *our_lock = string_copy(lock);
    list_string_append(task->held_locks, our_lock);
}

void armus_task_before_unlock(ArmusTask *task, const char *lock) {
    if (lock == NULL) {
        return;
    }
    char *result = list_string_remove(task->held_locks, lock);
    if (result) {
        free(result);
    } // XXX: otherwise warn
}

void armus_task_after_unlock(ArmusTask *task, const char *lock) {
    // do nothing
}


ArmusTask* armus_task_new(ArmusTaskSetBlocked set_blocked_func) {
    ArmusTask* result = (ArmusTask*) MALLOC(sizeof(ArmusTask));
    result->barrier = 0;
    result->held_locks = list_string_new();
    result->set_blocked_func = set_blocked_func;
    return result;
}

void armus_task_free(ArmusTask *task) {
    list_string_free_all(task->held_locks);
    free(task);
}
