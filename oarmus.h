#ifndef __OMP_ARMUS_H
#define __OMP_ARMUS_H
#ifdef __cplusplus
extern "C" {
#endif

#include "armus_task.h"
typedef void (*OmpSetLock)(omp_lock_t *lock);
typedef void (*OmpUnsetLock)(omp_lock_t *lock);
void armus_omp_set_lock(omp_lock_t *lock, OmpSetLock proceed);
void armus_omp_unset_lock(omp_lock_t *lock, OmpUnsetLock proceed);

#ifdef __cplusplus
}
#endif
#endif
